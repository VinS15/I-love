package com.chillcoding.mycuteheart.model

import com.chillcoding.mycuteheart.R

/**
 * Created by macha on 21/09/2017.
 */
data class MyAward(val img: Int = R.drawable.ic_menu_awards, val name: String = "Bonus", val score: Int = 0, val mode: Int = 0)
