package com.chillcoding.mycuteheart.model

/**
 * Created by macha on 02/08/2017.
 */
enum class MyFragmentId {
    ABOUT, AWARDS, SETTINGS, TOP;
}
