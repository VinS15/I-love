package com.chillcoding.mycuteheart.model

/**
 * Created by macha on 24/11/2017.
 */
class Score(val pseudo: String, val score: Int)